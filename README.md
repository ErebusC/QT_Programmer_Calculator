QT Programmer Calculator
========================

Function: 
---------

Normal mathmatical functions of a calculator and to convert numbers to the binary, hex or octal numbering systems.

Description/Background information
----------------------------------

Version 1
---------

This was originally the GUI build of the decimal converter found here: https://gitlab.com/ErebusC/small_projects/tree/master/cpp_projects/Decimal_converter. While creating this program however I decided to add standard mathmatical operations to increase its functionality, in doing so I created a GUI program that does not closely relate to that project. 

Version 2
---------

Version 2 is my second attempt at making a GUI calculator. It was created mainly for an assignment I had in my Event Driven Program class in college. Hence why it has a random timer to clear the result label and the unnecessary save function.

To Do List
----------

-[x] Completed Task -[ ] Incomplete Task

-[] Add a form for "advanced view".  

-[] Add more advanced mathematical operations

-[] Add conversion to binary, hex, octal.

-[] Add a help guide form, per my EDP classes specification

-[] Add math operations with binary numbers.

-[] Add keyboard functionality.

-[] Add "Help" and "View" items to the menu bar. 
