#ifndef INCLUDED_HEADERS_H
#define INCLUDED_HEADERS_H

#include <QDebug>
#include <QPushButton>
#include <QLabel>
#include <QString>
#include <QListView>
#include <QStringList>
#include <QStringListModel>
#include <QAbstractItemView>
#include <QVector>
#include <QMenuBar>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QTimer>
#include <QAction>

#endif // INCLUDED_HEADERS_H
